<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Asistencia Estudiantes</title>
</head>

<body>
    <h1>Reporte de asistencia de estudiantes</h1>
    @if ($errors->any())
        <p>{{ $errors }}</p>
        <hr>
    @endif

    @if (session('success'))
        <p>{{ session('success') }}</p>
        <hr>
    @endif
    <strong>El reporte será generado basado en el último archivo subido</strong>
    <hr>

    <form action="{{ route('reporte_download') }}" method="post">
        @csrf
        <input type="submit" value="Generar reporte">
    </form>
    <hr>
    <form action="{{ route('reporte_download_error') }}" method="post">
        @csrf
        <input type="submit" value="Generar reporte (errores)">
    </form>

    <hr>
    <a href="{{route('carga_index')}}">Subir archivo...</a>
    <a href="{{route('instrucciones')}}">Instrucciones</a>
</body>

</html>
