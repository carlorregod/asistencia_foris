<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Asistencia Estudiantes</title>
</head>

<body>

    <h1>Instrucciones</h1>

    <ol>
        <li>
            En enlace de <a href="{{route('carga_index')}}">Subir archivo...</a> deberá subir un archivo de extensión *txt el cual, deberá poseer la siguiente estructura
            <img src="{{asset('instrucciones/Subir_txt.png')}}" alt="Subir imágen" ><br>
            Por cada estudiante:
            <table border="1">
                Student XXXXXX
            </table>
            Donde XXXXXX es el nombre del estudiante, puede ser nombre completo.
            <table border="1">
                Presence XXXXXX numero_semana hora_inicio hora_final codigo_sala
            </table>
            Donde numero_semana corresponde al 1 si es lunes o 6 si es sábado de forma consecutiva. las horas inicio y final
            son las horas de entrada y salida a esa clase en formato hora (24 horas): minuto ejemplo, 22:15, y codigo_sala es
            el código clave de la sala en la que el alumno ha asistido.
            <br>&nbsp;
        </li>
        <li>
            En el enlace de <a href="{{route('reporte_index')}}">Reporte...</a> existe un botón que generará un resumen de asistencia. Notar que usted
            puede subir en el paso anterior, tantos archivos como desee, pero solamente podrá obtener el reporte del último archivo subido.
            <img src="{{asset('instrucciones/Genera_reporte.png')}}" alt="Subir imágen" />
        </li>
    </ol>

    <hr>
    <a href="{{route('carga_index')}}">Subir archivo...</a>
    <a href="{{route('reporte_index')}}">Reporte...</a>

</body>

</html>
