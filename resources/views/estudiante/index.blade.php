<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Asistencia Estudiantes</title>
</head>

<body>
    <h1>Subir archivo de asistencia al servidor</h1>
    @if ($errors->any())
        <p>{{ $errors }}</p>
        <hr>
    @endif

    @if (session('success'))
        <p>{{ session('success') }}</p>
        <hr>
    @endif


    <form action="{{ route('carga_archivo') }}" method="post" enctype="multipart/form-data">
        @csrf
        <table>
            <tr>
                <label for="fileup">Subir archivo .txt</label>
                <input type="file" name="fileup" id="fileup" required><br>
            </tr>
            <tr><input type="submit" value="Subir archivo al servidor">
            </tr>
        </table>
    </form>

    <hr>
    <p>Archivos subidos al servidor:</p>
    <ol>
    @foreach ($lista_archivos as $archivo)
        <li>{{$archivo}}</li>
    @endforeach
    </ol>
    <hr>

    <a href="{{route('reporte_index')}}">Reporte...</a>
    <a href="{{route('instrucciones')}}">Instrucciones</a>
</body>

</html>
