# Ubicación archivos importantes

Para regresar al readme [click acá](../README.md).

* * *
## Vistas

Éstas se alojan en `resources/views`.

Su definición se halla en  `routes/web.php` (a diferencia de Cake, Laravel exige definir las rutas)

El comando `php artisan route:list` muestra todas las rutas tanto para API como para web.

## Tests

Las pruebas de esting (que no son muchas) se hallan en `tests`.

## Configuraciones

Las configuraciones se efectúan a través de variables de entorno en el archivo `.env` o bien,de forma específica en el directorio `config`.

## Modelos

Los modelos se hallan en `app\Models`.

## Controladores

Los controladores se hallan en `app\Http\Controllers`.
