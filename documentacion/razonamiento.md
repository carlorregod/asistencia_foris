# Razonamiento


Para regresar al readme [click acá](../README.md).

## Elección php

Simplemente es el lenguaje con el que trabajo día a día junto al framework Laravel, aunque podría efectuarlo con python o java peroe stos últimos los utilizo más para asuntos de analítica y big data y no para desarrollo.

Dentro de los frameworks y pensando en escalabilidad al mediano-largo plazo, Laravel y Synfony son los más potentes y permiten levantar plataformas robustas ys eguras en poco tiempo, algo que cake php y CodeIgniter han quedado algo rezagadas en el tiempo.

## Elección de BD sobre txt

Es conveniente garantizar la persistencia de datos y por ello, se ha elegido un motor transaccional de base de datos. Laravel ene ste punto es tan versátil que se adapta a cualquier base de datos. Si bien este ejercicio se encuentra codificado para desplegar en SQLite, se puede de manera muy sencilla configurar otra base de datos, ejemplo, Postgres o MongoDB, debido al uso de migraciones. Ene sto nisiquiera DJango es tan potente y autoadaptable como Laravel.

## Usando MVC

Como la aplicación no es compleja pero además, conviene seguir buenas prácticas, se utilizó un orden MVC porque Laravel por defecto, trabaja sobre él pero recordemos que sigue siendo php puro, por ende, se pueden incorporar otros patrones de diseño (ejemplo, Singleton, factory, etc) los cuales no fueron necesarios en este paso. De hecho una elección correcta conlleva conocer 100% el negocio final, algo imposible para un postulante y además de contar con plazo límite para entrega.

## ¿Porqué no otro Lenguaje o stack?

Sé bien que usan fuertemente Cake, pero de acá dos aspectos

* Cake si bien es un buen framework, no tiene muchas opciones (las cuales sí requiere instalar un plugin o similar), además de utilizar librerías ya obsoletas como phpmail.

* Laravel posee una gran comunidad muy activa, más que Synfony (y eso que Synfony es la base de Laravel como lo es Debian de Ubuntu) y eso ha efectuado que existan una infinidad de recursos que ya vienen por defecto o que se instalan con composer.

* Pese a ser meme, php es tan potente como otros lenguajes pero es adaptable a un código imeprativo, interpretativo, declarativo u OO. En eso es muy similar a Python o Javascript (salvo sintaxis claro).

* El tema de la Base de datos para este ejercicio es trivial, pero hay sistemas que funcionan mejor en ciertos motores de BD. En eso, tanto php como Laravel conviven bien con los motores más populares, como MariaDB, SQL Server, Oracle (este necesita un plugin), Casandra, DynamoDB y otros.

## Dos vistas, la clave es el back end

(He visto que muchos lo escriben como backend, esto ya es duda personal pero ¿cómo se escribe enr ealdiad?)

* No se necesita más, una pantalla para subir archivos
* Otra para la descarga del reporte y/o errores
* Estoy consciente que el planteamiento no pedía lista de archivos subidos al servidor + reporte de posibles errores, pero el sistema está pensado en escalar, por eso, solo se mostró estas opciones como curiosidad simplemente, al cliente final esto iría comentado quizás o se dejaría esto en un branch aparte.
