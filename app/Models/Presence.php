<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    use HasFactory;
    protected $guarded = [];

    //Garantizar relación con asistencia
    public function presences()
    {
        return $this->belongsTo('App\Estudiante'); //"Pertenece a..."
    }
}
