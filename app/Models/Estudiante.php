<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    use HasFactory;

    protected $guarded = [];

    //Garantizar relación con asistencia
    public function presences()
    {
        return $this->hasMany('App\Presence'); // "Tiene uno o varios..."
    }


}
