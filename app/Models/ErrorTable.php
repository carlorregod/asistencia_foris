<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErrorTable extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table='asistencia_errores';
}
