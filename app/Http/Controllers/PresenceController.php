<?php

namespace App\Http\Controllers;

use App\Models\ErrorTable;
use App\Models\Estudiante;
use App\Models\Presence;
use DB;
use Rap2hpoutre\FastExcel\FastExcel;


class PresenceController extends Controller
{
    # Vista de reporte
    public function index()
    {
        return view('presence.index');
    }

    #Para generar archivo de reporte
    public function download()
    {
        /* ------------------------ Paso de obtención de data ----------------------- */
        $archivo_a_procesar = Presence::latest()->value('nombrearchivo');
        $presence = Estudiante::where('estudiantes.nombrearchivo',$archivo_a_procesar)->leftJoin('presences as p','estudiantes.id','p.estudiante_id')->
            select(DB::raw("Nombre as nombre, dia_semana, sala_codigo, hora_inicio_detección, hora_final_detección"))->get();

        if($presence->count() == 0)
        {
            return redirect()->back()->withErrors('No hay elementos cargados en el sistema. Favor ir a cargar archivo (esto puede ocurrir también en el caso que todos los registros estén con errroes)');
        }
        $response = $resp = $aux = $response_ = array();
        foreach ($presence as $clave => $p) {
            $presence[$clave]->diferencia =(strtotime($p->hora_final_detección) - strtotime($p->hora_inicio_detección))/60; #Diferencia horaria en minutos
            if(isset($response[$p->nombre]))
            {
                $response[$p->nombre]['diferencia'] += $presence[$clave]->diferencia;
                $response[$p->nombre]['cuentadia'] ++;
            }
            else
            {
                $response[$p->nombre]['diferencia'] = is_null($p->hora_final_detección) ? 0 : $presence[$clave]->diferencia;
                $response[$p->nombre]['cuentadia'] = is_null($p->hora_final_detección) ? 0 : 1;
                $response[$p->nombre]['nombre'] = $p->nombre;
            }
        }
        # Un último foreach para formatear la respuesta
        unset($presence);
        foreach ($response as $key => $row) {
            $aux[$key] = $row['diferencia'];
        }
        array_multisort($aux, SORT_DESC, $response);
        foreach($aux as $key=>$row) {
            $response_[$key] = $response[$key];
        }

        foreach ($response_ as $clave => $r) {
            if($r['diferencia']==0)
            {
                $resp[]['Report:'] = "{$r['nombre']}: {$r['diferencia']} minutes";
            }
            elseif($r['cuentadia']==1)
            {
                $resp[]['Report:'] = "{$r['nombre']}: {$r['diferencia']} minutes in {$r['cuentadia']} day";
            }
            else
            {
                $resp[]['Report:'] = "{$r['nombre']}: {$r['diferencia']} minutes in {$r['cuentadia']} days";
            }
        }
        /*
        Ejemplo de response:
            Marco: 142 minutes in 2 days
            David: 104 minutes in 1 day
            Fran: 0 minutes
        */
        $resp = collect($resp);
        return (new FastExcel($resp))->download('reporte.csv');
    }

    public function download_error()
    {
        $nombrearchivo = Presence::latest()->value('nombrearchivo');
        $errorTable = ErrorTable::where('nombrearchivo',$nombrearchivo)->get();
        if($errorTable->count() == 0)
        {
            return redirect()->back()->withErrors('No hay elementos con errores.');
        }
        return (new FastExcel($errorTable))->download('reporte_error.csv');
    }
}
