<?php

namespace App\Http\Controllers;

use App\Models\Estudiante;
use App\Models\Presence;
use App\Models\ErrorTable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class EstudianteController extends Controller
{
    /* ------------------------- Atributos para la clase ------------------------ */
    protected $nombre;


    # Carga de arhcivo
    public function index()
    {
        $lista_archivos = Storage::disk('asistencia')->allFiles();
        return view('estudiante.index', compact('lista_archivos'));
    }

    public function instrucciones()
    {
        return view('estudiante.instrucciones');
    }

    public function load()
    {
        self::cleanDB(); #Limpieza de BD
        $file = request()->file('fileup');
        if($file == null) {
			return redirect()->back()->withErrors('No se ha subido archivo y/o éste se encuenta vacío');
		}
        $nombre = $file->getClientOriginalName();
        $arr_nombre = explode('.',$nombre);
        //Etapa cero ¿hay archivo?
        if(!in_array($arr_nombre[count($arr_nombre)-1],['txt']) || $nombre == '') { //Acá se está validando que sea un archivo de texto
            return redirect()->back()->withErrors('Archivo subido no es tipo txt');
        }
        $nombre_nuevo = date('Y-m-d H-i-s')."-".$nombre;
        $this->nombre=$nombre_nuevo;
        $contenido = File::get($file);
        $contents = explode("\r\n",$contenido);
        #Se valida la data previamente
        $response = $this->validate_data($contents);
        // dd($response['estudiantes']);
        # Guardando respuestas en la BD
        DB::beginTransaction();
        try
        {
            foreach($response['estudiantes'] as $student) {
                Estudiante::updateOrCreate($student);
            }
            // Estudiante::updateOrCreate($response['estudiantes']); #Para insert masivo sirve aunque es mejor efectuarlas de a una con create
            # Prelimpieza de búsqueda de foráneos para los estudiantes
            $response['presencia'] = $this->foreign_key_presents_student($response['presencia']);
            Presence::insert($response['presencia']);
            ErrorTable::insert($response['error']);
            DB::commit();

            Storage::disk('asistencia')->put($nombre_nuevo, trim($contenido)); #El viejo truco de respaldar los archivos subidos
            return redirect()->back()->with("success","Subida de archivo lista");

        }
        catch(\Exception $e)
        {
            DB::rollBack();
            return redirect()->back()->withErrors("Ocurrió un error: ".$e->getMessage());
        }



    }

    /* -------------------------------------------------------------------------- */
    /*                Métodos anexos de apoyo de validación de data               */
    /* -------------------------------------------------------------------------- */

    # validador de data
    private function validate_data(array $contents) : array
    {
        $error = $estudiante = $presencia = array();
        foreach ($contents as $clave => $content) {
            if($clave == count($contents)-1)
            {
                break;
            }
            $row = explode(" ",$content);
            # revisando estructura del archivo a subir
            if(!in_array($row[0],["Student","Presence"]))
            {
                $error[] = array(
                    'error'=>"Línea no cuenta con clasificación primaria de Student o Presence",
                    'data'=>$content,
                    'nombrearchivo'=>$this->nombre,
                );
                continue;
            }
            #Guardar la data para luego, almacenarla.
            switch($row[0])
            {
                case "Student":
                    if(count($row) >=2)
                    {
                        $nombre = '';
                        for($i = 1; $i < count($row); $i++)
                        {
                            $nombre .= $row[$i]." ";
                        }
                        $estudiante[] = trim($nombre);
                    }
                    else
                    {
                        $error[] = array(
                            'error'=>"Línea no cuenta con nombre",
                            'data'=>$content,
                            'nombrearchivo'=>$this->nombre,
                        );
                    }
                    break;

                case "Presence":
                    $regexHora = '/^([0-1][0-9]|2[0-3])(:)([0-5][0-9])$/';
                    if(count($row) >=6 && in_array($row[count($row)-4],array(1,2,3,4,5,6,7)) &&
                    preg_match($regexHora, $row[count($row)-3], $m1) && preg_match($regexHora, $row[count($row)-2], $m2))
                    {
                        if( (strtotime($row[count($row)-2]) - strtotime($row[count($row)-3]))/60 >= 5) #Más de 5 minutos
                        {
                            $nombre = '';
                            for($i = 1; $i <= count($row)-5; $i++)
                            {
                                $nombre .= $row[$i]." ";
                            }
                            $presencia[]=array(
                                'nombre'=>trim($nombre),
                                'dia_semana'=>$row[count($row)-4],
                                'hora_inicio_detección'=>$row[count($row)-3],
                                'hora_final_detección'=>$row[count($row)-2],
                                'sala_codigo'=>$row[count($row)-1],
                                'nombrearchivo'=>$this->nombre,
                            );
                        } else {
                            $error[] = array(
                                'error'=>"Registro omitido por contar con menos de 5 minutos",
                                'data'=>$content,
                                'nombrearchivo'=>$this->nombre,
                            );
                        }
                    }
                    else
                    {
                        $error[] = array(
                            'error'=>"Línea no cuenta con uno o más datos mínimos para la asistencia y/o data no respeta validaciones iniciales",
                            'data'=>$content,
                            'nombrearchivo'=>$this->nombre,
                        );
                    }
                    break;

                default:
                    $error[] = array(
                        'error'=>"Línea no cuenta con uno o más datos mínimos para la asistencia",
                        'data'=>$content,
                        'nombrearchivo'=>$this->nombre,
                    );

            }

        }

        #Prelimpieza de unicos
        $estudiante =array_unique($estudiante);
        $estudiantes = array();
        foreach($estudiante as $clave=>$e)
        {
            if(!is_null(Estudiante::where('Nombre',$e)->first()))
            {
                unset($estudiante[$clave]);
            }
            else
            {
                $estudiantes[]=array('Nombre'=>$e,
                    'nombrearchivo'=>$this->nombre
                );
            }
        }

        return compact('error','estudiantes','presencia');
    }

    # Hallando clave foránea de presence luego de isnertar a los estudiantes
    private function foreign_key_presents_student(array $presencia) :array
    {
        foreach ($presencia as $index => $p) {
            $presencia[$index]['estudiante_id'] = Estudiante::where('Nombre',$p['nombre'])->pluck('id')->first();
            unset($presencia[$index]['nombre']);

        }

        return $presencia;
    }

    # Truncando tablas, esto es para la solución del ejercicio, la idea es garantizar persistencia de data
    private static function cleanDB() : void
    {
        Estudiante::truncate();
        ErrorTable::truncate();
        Presence::truncate();
    }


}
