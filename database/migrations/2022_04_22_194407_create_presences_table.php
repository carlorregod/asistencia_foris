<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presences', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->unsignedBigInteger('estudiante_id');
            $table->foreignId('estudiante_id');
            $table->integer('dia_semana');
            $table->time('hora_inicio_detección');
            $table->time('hora_final_detección');
            $table->string('sala_codigo',50);
            $table->string('nombrearchivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presences');
    }
};
