<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Ejercicio actividad entrevista para Foris

## Requerimientos

### Mínimos

* php 8.0 o superior
* composer

### Ideales y opcionales

* Contenedor Docker basado en php 8.1 + composer. Servidor nginx/apache2 y configuración para desplegar página desde asistencia/public. Caso contrario, un servidor dedicado, XAMPP o similar.

* Contar con conectividad a una base de datos transaccional (ejemplo, mysql).

* Si se satisface el segundo punto antes mencionado, la configuración de la base de datos se efectúa en el archivo .env según [este enlace](https://laravel.com/docs/9.x/database#configuration).

### Despliegue

* Dentro de su servidor, ejecute
```
git clone https://gitlab.com/carlorregod/asistencia_foris.git asistencia
cd asistencia
cp .env.example .env
composer install
php artisan key:generate
php artisan migrate
php artisan serve #Este último para desplegar el servidor en localhost:8000 de manera opcional)
```

* Dentro del aplicativo desplegado se encuentra un manual de uso nivel usuario.

* Puede correr los test del sistema con

```
php artisan test
```

* ( ͡° ͜ʖ ͡°)

## Explicación de razonamiento

Puede encontrarlo en [este enlace](documentacion/razonamiento.md).

Archivos importantes (ubicación) en [este enlace](documentacion/ubicacion.md).

Enunciado del problema [click acá](documentacion/E1_entrevista_foris.md).


