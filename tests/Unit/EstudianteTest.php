<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Controllers\EstudianteController;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


class EstudianteTest extends TestCase
{

    /**
     * Testeando accesos web.
     * @test
     *
     * @return void
     */
    public function http_test()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response = $this->get('reporte/instrucciones');
        $response->assertStatus(200);
    }

    /**
     * Testeando descarga de csv de reportes.
     * @test
     *
     * @return void
     */

    public function fileUploads()
    {
        $fileSize = 4048; // 4mb
        $fileName = "testing.txt";
        $fileNameNuevo = date('Y-m-d H-i-s')."-testing.txt";
        Storage::fake('asistencia');

        $response = $this->json('POST', '/', [
            'fileup' => UploadedFile::fake()->create($fileName, $fileSize)
        ]);

        // ¿¿Archivo está guardado??
        Storage::disk('asistencia')->assertExists($fileNameNuevo);

        // Archivo no debe existir...
        Storage::disk('asistencia')->assertMissing($fileName);

        $response->assertStatus(302);
    }




}
