<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
# Sitio para cargar archivo txt
Route::get('/', 'EstudianteController@index')->name('carga_index');
Route::post('/', 'EstudianteController@load')->name('carga_archivo');

Route::get('reporte','PresenceController@index')->name('reporte_index');
Route::post('reporte','PresenceController@download')->name('reporte_download');
Route::post('reporte/errores','PresenceController@download_error')->name('reporte_download_error');

Route::get('reporte/instrucciones','EstudianteController@instrucciones')->name('instrucciones');
